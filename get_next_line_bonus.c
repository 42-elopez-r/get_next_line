/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 22:34:40 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/15 15:10:30 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"
#include <unistd.h>
#include <stdlib.h>

static char		*join(char *s1, char *s2, int max_index)
{
	char	*joined;
	int		i;
	int		j;

	if ((joined = malloc((s1 ? ft_strlen(s1) : 0) + ft_strlen(s2) + 1)))
	{
		i = 0;
		while (s1 && s1[i])
		{
			joined[i] = s1[i];
			i++;
		}
		j = 0;
		while (s2[j] && j <= max_index)
			joined[i++] = s2[j++];
		joined[i] = '\0';
		if (s1)
			free(s1);
	}
	return (joined);
}

static ssize_t	read_buf(char *buf, int fd)
{
	ssize_t			readed;

	readed = 0;
	if (chr_pos(buf, '\n') == -1)
	{
		if ((readed = read(fd, buf, BUFFER_SIZE)) < 0)
			return (-1);
		buf[readed] = '\0';
	}
	return (readed);
}

int				get_next_line(int fd, char **line)
{
	static char		buf[4096][BUFFER_SIZE + 1] = {{0}};
	int				nl;

	if (!line || fd < 0)
		return (-1);
	*line = NULL;
	if (buf[fd][0] && chr_pos(buf[fd], '\n') == -1)
		*line = join(*line, buf[fd], ft_strlen(buf[fd]));
	while (1)
	{
		if (read_buf(buf[fd], fd) == -1)
			return (-1);
		nl = chr_pos(buf[fd], '\n');
		*line = join(*line, buf[fd], nl != -1 ? nl - 1 : ft_strlen(buf[fd]));
		if (nl != -1)
		{
			trim_until_chr(buf[fd], '\n');
			return (1);
		}
		else if (ft_strlen(buf[fd]) < BUFFER_SIZE)
		{
			buf[fd][0] = '\0';
			return (0);
		}
	}
}
