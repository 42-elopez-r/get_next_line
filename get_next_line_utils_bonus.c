/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:35:05 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/13 20:27:32 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"
#include <stdlib.h>

size_t	ft_strlen(const char *s)
{
	int acm;

	acm = 0;
	while (s[acm])
		acm++;
	return (acm);
}

int		chr_pos(char *s, char c)
{
	int i;

	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

char	*copy_until_chr(char *s, char c)
{
	int		len;
	int		i;
	char	*copy;

	len = 0;
	while (s[len] && s[len] != c)
		len++;
	if ((copy = malloc(len + 1)))
	{
		i = 0;
		while (i < len)
		{
			copy[i] = s[i];
			i++;
		}
	}
	return (copy);
}

void	trim_until_chr(char *s, char c)
{
	int		i;
	int		j;

	i = 0;
	while (s[i] && s[i] != c)
		i++;
	j = 0;
	i++;
	while (s[i])
		s[j++] = s[i++];
	s[j] = '\0';
}
